package arth

import "testing"

func TestHello(t *testing.T) {
	want := 10
	if got := add(3, 7); got != want {
		t.Errorf("add(3, 7) = %d, want %d", got, want)
	}
}
